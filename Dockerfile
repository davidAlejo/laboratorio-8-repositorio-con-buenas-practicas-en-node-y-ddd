FROM node:15-alpine

ENV NODE-ENV = laboratorio7

WORKDIR /app

COPY ["package.json", "package-lock.json", "./"]

RUN npm install --laboratorio7

COPY . .

CMD ["node", "index.js"]