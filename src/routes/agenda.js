import express from 'express'
import { persons } from '../utils/mockup'
import { crearPersona, eliminarPersona } from '../components/personas/controller'
import validationHandler from '../utils/middlewares/validationHandler'
import { createAuthUserSchema } from '../components/personas/domain/auth'

const router = express.Router()

router.get('/', async (_, response, bext) => {
  response.send('<h1>Hello World!<h1>')
})

router.get('/api/persons', (request, response) => {
  response.json(persons)
})
router.get('/info', (request, response) => {
  const cantidad = persons.length
  const fecha = new Date()
  fecha.toDateString()

  response.send('<p>Phone has info for ' + cantidad + ' people</p> <p>' + fecha + '</p>')
})

router.get('/api/persons/:id', (request, response) => {
  const idperson = request.params.id
  const person = persons.find(pers => pers.id == idperson)

  if (person) {
    response.json(person)
  } else {
    response.status(404).end('No se puedo encontrar su busqueda')
  }
})

router.delete('/api/persons/delete/', eliminarPersona)

router.post(
  '/api/persons',
  validationHandler(createAuthUserSchema),
  crearPersona)

export default router
