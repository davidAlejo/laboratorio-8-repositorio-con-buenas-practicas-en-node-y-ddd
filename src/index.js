import express from 'express'
import apiAgenda from './routes/agenda.js'
import morgan from 'morgan'
import helmet from 'helmet'

const app = express()
app.use(morgan('dev'))
app.use(express.json())
app.use('/', apiAgenda)
app.use(helmet())

app.use((req, res, next) => {
  res.status(404).send('<h1 style="color:red;">La url donde usted trata de ingresar no existe</h1>')
})

const PORT = process.env.PORT || 3000

// const PORT = 8000
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})
