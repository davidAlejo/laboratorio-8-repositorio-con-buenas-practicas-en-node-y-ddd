import Joi from 'joi'

export const createAuthUserSchema = Joi.object({
  name: Joi.string().alphanum().required(),
  number: Joi.string().required()
})
