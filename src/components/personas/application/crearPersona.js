/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoPersonRepository')} obj.PersonRepository
 */

export default ({ PersonRepository }) => {
  return async ({ name, number }) => {
    // if (!email) throw new Error('validation failed')
    // return UserRepository.getByEmail()

    const newPerson = {
      name: name,
      number: number
    }

    return await PersonRepository.add(newPerson)

    /* const persona = request.body
    const personExist = persons.find(person => person.name === persona.name)
    if (persona.name === '' || persona.number === '') {
      response.status(406).json({ Error: 'El campo name o number no tienen datos' })
    } else if (personExist) {
      response.status(406).json({ Error: 'El nombre ya existe' })
    } else {
      // const num = Math.floor((Math.random() * (100000 - 4)) + 4)
      const newPerson = {
        name: persona.name,
        number: persona.number
      }
      const id = await mongo.create('persons', newPerson)
      response.status(201).json({
        id,
        ...newPerson
      })
    } */
  }
}
